#!/usr/bin/env python3


from bs4 import BeautifulSoup
import requests

url = "https://www.irelandwestairport.com/flight_information"
img_host = "https://www.irelandwestairport.com"
filename = "/var/www/html/tdl-test/index.html"

try:
	r = requests.get(url)
except:
	print("Error getting URL")

try:
	soup = BeautifulSoup(r.content, 'html.parser')
except:
	print("Error parsing contents")


for img in soup.find_all('img'):
    img_url = img['src']
    if (img_url.startswith('/')):
        img_url = img_url.replace(img_url, img_host + img_url)
    img['src'] = img_url

for css_link in soup.find_all('link'):
    css_url = css_link['href']
    if (css_url.startswith('/')):
        css_url = css_url.replace(css_url, img_host + css_url)
    css_link['href'] = css_url

for script_link in soup.find_all('script'):
    # Some script tags don't have a src attribute. Let's skip them.
    if "src" in script_link.attrs:
        js_url = script_link['src']
        if (js_url.startswith('/')):
            js_url = js_url.replace(js_url, img_host + js_url)
        script_link['src'] = js_url

try:
	fh = open(filename, "w")
	fh.write(str(soup))
	fh.close()
except:
	print("Problem writing file")
